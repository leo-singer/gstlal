#!/usr/bin/env python
#
# Copyright (C) 2011  Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

### This program is designed to be placed in the cgi-bin directory of the user's
### public_html directory on the cluster that is running the gstlal inspiral low
### latency analysis
###
### Review Status
### -------------
###
### +----------------------------------------------------+---------------------------------------------+------------+
### | Names                                              | Hash                                        | Date       |
### +====================================================+=============================================+============+
### | Florent, Sathya, Sarah Caudill, Jolien, Kipp, Chad | 9d1ef72ae9530cefbfb822260e4391a181a17c0d    | 2015-09-11 |
### +----------------------------------------------------+---------------------------------------------+------------+
###
###
### USAGE:
### ------
###
### This program is never meant to be executed by a user, but rather on a
### webserver via a url such as::
###
###	 https://hostname.domain/path/to/cgi-bin/gstlal_llcbcsummary?id=<start>,<stop>&dir=/path/to/analysis/directory&ifos=<IFOS>
###
### e.g.,::
###
### 	https://ldas-jobs.ligo.caltech.edu/~gstlalcbc/cgi-bin/gstlal_llcbcsummary?id=0001,0010&dir=/home/gstlalcbc/engineering/5/bns_trigs_40Hz&ifos=H1,L1,V1
###
### \<start\> and \<stop\> are the 4 digit id numbers corresponding to the first and
### last job, respectively.
###



import sys
import cgi
import cgitb
cgitb.enable()
import os
os.environ["MPLCONFIGDIR"] = "/tmp"
import time
import urlparse
from gstlal import llweb

form = cgi.parse()

if "dir" not in form and "nagios" in form:
	form = urlparse.parse_qs(open("gstlalcbc_analysis.txt").readline().strip())
	web = llweb.GstlalWebSummary(form = form)
	web.nagios()
	sys.exit()
elif not form:
	form = urlparse.parse_qs(open("gstlalcbc_analysis.txt").readline().strip())
web = llweb.GstlalWebSummary(form = form)

# Header
print >>sys.stdout, 'Cache-Control: no-cache, must-revalidate'
print >>sys.stdout, 'Expires: Mon, 26 Jul 1997 05:00:00 GMT'
print >>sys.stdout, 'Content-type: text/html\r\n'

print """
<html>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
<meta http-equiv="refresh" content="300">
<title>GstLAL CBC</title>
"""

print llweb.css

print """
<div class=topbox> 
        <table>
        <tr><th rowspan=2><img width=100px src="http://www.lsc-group.phys.uwm.edu/cgit/gstlal/plain/gstlal/doc/gstlal.png"></th><th>Nodes</th><th>IFOs</th><th>GPS</th><th>Date</th><th>Status</th></tr>
        <tr><td>%s</td><td>%s</td><td>%d</td><td>%s</td><td>%s</td></tr>
        </table>
<hr>
</div>
""" % (",".join([min(web.registry.keys()), max(web.registry.keys())]), "".join(sorted(web.ifos)), int(llweb.now()), time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.localtime()), web.status()[1])


cnt = 0

if os.path.isfile("%s/sensitivity_base64.txt" % web.directory):
	print """
	<div class="tabs">
		<div class=tab>
			<input type="radio" id="tab-%d" name="tab-group-1" checked>
			<label for="tab-%d">%s</label>
			<div class="content"><img src="data:image/png;base64,%s"></img></div>
		</div>
	""" % (cnt, cnt, 'Analysis Sensitivity', open("%s/sensitivity_base64.txt" % web.directory).readline())
	cnt+=1

	print """
	<div class=tab>
		<input type="radio" id="tab-%d" name="tab-group-1">
	""" % cnt
else:
	print """
	<div class="tabs">
		<div class=tab>
			<input type="radio" id="tab-%d" name="tab-group-1" checked>
	""" % cnt

print """
		<label for="tab-%d">Summary</label>
		<div class="content">
			<table>
				<tr><th>Latency</th><th>Time since last</th><th>Live time</th></tr>
				<tr><td>%s     </td><td>%s             </td><td>%s       </td></tr>
			</table>
		</div>
	</div>
""" % (cnt, web.latency(), web.time_since_last(), web.average_up_time())

cnt+=1

for dt in ("latency_history","snr_history","bank","ram_history"):
	print """
	<div class=tab>
	<input type="radio" id="tab-%d" name="tab-group-1">
	<label for="tab-%d">%s</label>
	<div class="content">%s</div>
	</div>
	""" % (cnt, cnt, dt.replace("_", " "), web.plot(dt))
	cnt +=1

for ifo in web.ifos:
	for dt in ("%s/state_vector_on_off_gap" % ifo,):
		print """
	<div class=tab>
		<input type="radio" id="tab-%d" name="tab-group-1">
		<label for="tab-%d">%s</label>
		<div class="content">%s</div>
	</div>
		""" % (cnt, cnt, dt.replace("_", " "), web.plot(dt, ifo = ifo))
		cnt+=1

# This plot takes too long to load for a summary page.
#print """
#	<div class=tab>
#	<input type="radio" id="tab-%d" name="tab-group-1">
#	<label for="tab-%d">%s</label>
#	<div class="content">%s</div>
#	</div>
#	""" % (cnt, cnt, "likelihood CCDF", web.plot("marginalized_likelihood"))
#cnt+=1

print """
</div>
<div class=topbox>
	Node links:
"""

for id in sorted(web.registry):
	url = os.path.join(web.directory, "%s/likelihood.xml" % (id,))
	print """
	<a target="_blank" href="gstlalcbcnode?dir=%s&id=%s,%s&url=%s&ifos=%s">%s</a>
	"""  % (web.directory, id, id, url, ",".join(sorted(web.ifos)), id.strip())


print
"""
</div>

</html>
</body>
"""
