%define gstreamername gstreamer1

Name: @PACKAGE_NAME@
Version: @PACKAGE_VERSION@
Release: 1%{?dist}
Summary: GSTLAL
License: GPL
Group: LSC Software/Data Analysis
Requires: avahi
Requires: avahi-glib
Requires: avahi-ui-tools
Requires: fftw >= 3
Requires: glue >= @MIN_GLUE_VERSION@
Requires: gobject-introspection >= @MIN_GOBJECT_INTROSPECTION_VERSION@
Requires: gsl
Requires: %{gstreamername} >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-bad-free
Requires: %{gstreamername}-plugins-base >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-good >= @MIN_GSTREAMER_VERSION@
# FIXME:  add this when it becomes available, and figure out what its name
# is on .rpm based systems
#Requires: %{gstreamername}-python3-plugin-loader
# FIXME:  add this when it becomes available
#Requires: %{gstreamername}-rtsp-server >= @MIN_GSTREAMER_VERSION@
Requires: lal >= @MIN_LAL_VERSION@
Requires: lalburst >= @MIN_LALBURST_VERSION@
Requires: lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: lalinspiral >= @MIN_LALINSPIRAL_VERSION@
Requires: lalsimulation >= @MIN_LALSIMULATION_VERSION@
Requires: numpy > @MIN_NUMPY_VERSION@
Requires: orc >= @MIN_ORC_VERSION@
Requires: python >= @MIN_PYTHON_VERSION@
Requires: python-%{gstreamername}
Requires: python-gobject >= @MIN_PYGOBJECT_VERSION@
Requires: python2-lal >= @MIN_LAL_VERSION@
Requires: python-ligo-lw >= @MIN_LIGO_LW_VERSION@
Requires: python2-ligo-segments >= @MIN_LIGO_SEGMENTS_VERSION@
Requires: scipy
Requires: zlib
BuildRequires: doxygen >= @MIN_DOXYGEN_VERSION@
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= @MIN_GOBJECT_INTROSPECTION_VERSION@
BuildRequires: graphviz
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= @MIN_GTK_DOC_VERSION@
BuildRequires: %{gstreamername}-devel >= @MIN_GSTREAMER_VERSION@
BuildRequires: %{gstreamername}-plugins-base-devel >= @MIN_GSTREAMER_VERSION@
# FIXME:  add this when it becomes available
#BuildRequires: %{gstreamername}-rtsp-server-devel >= @MIN_GSTREAMER_VERSION@
BuildRequires: lal-devel >= @MIN_LAL_VERSION@
BuildRequires: lalburst-devel >= @MIN_LALBURST_VERSION@
BuildRequires: lalinspiral-devel >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
BuildRequires: lalsimulation-devel >= @MIN_LALSIMULATION_VERSION@
BuildRequires: numpy >= @MIN_NUMPY_VERSION@
BuildRequires: orc >= @MIN_ORC_VERSION@
BuildRequires: pkgconfig >= @MIN_PKG_CONFIG_VERSION@
BuildRequires: python-devel >= @MIN_PYTHON_VERSION@
# needed for gstpythonplugin.c remove when we remove that plugin from gstlal
BuildRequires: pygobject3-devel >= @MIN_PYGOBJECT_VERSION@
BuildRequires: python2-lal >= @MIN_LAL_VERSION@
BuildRequires: zlib-devel
Source: @PACKAGE_NAME@-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version}
Requires: fftw-devel >= 3
Requires: gsl-devel
Requires: %{gstreamername}-devel >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-base-devel >= @MIN_GSTREAMER_VERSION@
Requires: lal-devel >= @MIN_LAL_VERSION@
Requires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
Requires: lalsimulation-devel >= @MIN_LALSIMULATION_VERSION@
Requires: lalburst-devel >= @MIN_LALBURST_VERSION@
Requires: lalinspiral-devel >= @MIN_LALINSPIRAL_VERSION@
Requires: python-devel >= @MIN_PYTHON_VERSION@
%description devel
This package contains the files needed for building gstlal-based plugins
and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gir-*/*
%{_datadir}/gstlal
%{_datadir}/gtk-doc/html/gstlal-*
%{_docdir}/gstlal-*
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*
%{_libdir}/gstreamer-@GSTREAMER_RELEASE@/*.so
%{_libdir}/gstreamer-@GSTREAMER_RELEASE@/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-@GSTREAMER_RELEASE@/*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*
